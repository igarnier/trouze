(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_crypto

module Contract_hash =
  struct
    let contract_hash = "\002\090\121" (* KT1(36) *)

    include Blake2B.Make(Base58)(struct
                let name = "Contract_hash"
                let title = "A contract ID"
                let b58check_prefix = contract_hash
                let size = Some 20
              end)
  end

type implicit = Signature.public_key_hash
type originated = Contract_hash.t

type contract =
  | Implicit of implicit
  | Originated of originated

type block_hash = Block_hash.t
type block_level = int

type block =
  | Block_hash of block_hash
  | Block_level of block_level

let implicit_of_string s =
  Signature.Public_key_hash.of_string_exn s
let implicit_to_string i =
  Signature.Public_key_hash.to_string i

let originated_of_string s =
  Contract_hash.of_string_exn s
let originated_to_string k =
  Contract_hash.to_string k

let block_hash_of_string s =
  Block_hash.of_string_exn s
let block_hash_to_string bh =
  Block_hash.to_string bh
