open Minicli

open Rewards

let main () =
  let _argc, args = CLI.init () in
  let tz1   = CLI.get_string [ "--tz1" ] args in
  let start = CLI.get_int [ "--cycle-start" ] args in
  let stop  = CLI.get_int [ "--cycle-stop" ] args in
  let share  = CLI.get_float [ "--delegate-share" ] args in
  Printf.printf "tz1: %s\nstart: %d\nstop: %d\nshare: %f\n"
    tz1 start stop share

let _ =
  main ()
