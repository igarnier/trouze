(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type constants = {
  preserved_cycles : int ;
  blocks_per_cycle : int ;
  blocks_per_roll_snapshot : int
}

type head_info = {
  current_cycle : int ;
  current_cycle_position : int ;
  current_level : int
}

type 'mutez rights_info = {
  staking_balance : 'mutez ;
  delegators : Tztypes.contract list ;
  delegated_balance : 'mutez
}

type 'mutez frozen_balance = {
    deposit : 'mutez ;
    fees : 'mutez ;
    rewards : 'mutez
}

(* Printing *)

let print_constants (constants : constants) =
  let { preserved_cycles ;
        blocks_per_cycle ;
        blocks_per_roll_snapshot } = constants in
  Printf.printf
     "Current chain constants:\npreserved_cycles=%d\nblocks_per_cycle=%d\nblocks_per_roll_snapshot=%d\n%!"
       preserved_cycles blocks_per_cycle blocks_per_roll_snapshot

let print_head_info (head_info : head_info) =
  let { current_cycle; current_cycle_position; current_level } = head_info in
  Printf.printf
    "Current level: %d\nCurrent cycle: %d\nCurrent cycle position: %d\n%!"
    current_level current_cycle current_cycle_position

let print_rewards_info delegate cycle =
  let delegate = Tztypes.implicit_to_string delegate in
  Printf.printf "Computing rewards due by delegate %s at cycle %d\n%!"
    delegate cycle

(* Computing rewards *)

module type Tezos_view_sig =
sig
  val constants : Tztypes.block -> constants
  val head_info : unit -> head_info

  val balance_at_block : Tztypes.block -> Tztypes.contract -> Z.t
  val frozen_balance_of_cycle : int -> Tztypes.implicit -> Z.t frozen_balance
  val stake : Tztypes.block -> Tztypes.implicit -> Z.t rights_info
  val snapshot_block_of_cycle : int -> Tztypes.block_level
end

module Compute(View : Tezos_view_sig) =
struct

  let first_block_of_cycle (cycle_length : int) (cycle : int) =
    cycle * cycle_length + 1

  let rewards_of_cycle (delegate : Tztypes.implicit) (cycle : int) =
    let frozen_balance = View.frozen_balance_of_cycle cycle delegate in
    Z.add frozen_balance.fees frozen_balance.rewards

  let compute_rights_at_block (block : Tztypes.block) (delegate : Tztypes.implicit) =
    let { staking_balance ; delegators ; delegated_balance } = View.stake block delegate in
    let delegators =
      List.map (fun contract ->
          let b = View.balance_at_block block contract in
          match contract with
          | Tztypes.Implicit _ -> Pervasives.failwith "error: implicit delegators not handled yet"
          | Tztypes.Originated kt1 -> (kt1, Z.to_float b)
        ) delegators in
    (Z.to_float staking_balance,
     delegators,
     Z.to_float delegated_balance)

  let compute_rewards_for_cycle (delegate : Tztypes.implicit) (cycle : int) (percentage : float) =
    let head_info = View.head_info () in
    let constants = View.constants Tztypes.(Block_level head_info.current_level) in
    print_constants constants ;
    print_head_info head_info ;
    if cycle >= head_info.current_cycle then
      failwith "Cycle not present"
    else if percentage < 0.0 || percentage >= 1.0 then
      failwith "Invalid parameter: delegate share"
    else
      let rewards = rewards_of_cycle delegate cycle in
      let float_rewards_for_delegators = percentage *. (Z.to_float rewards) in
      let snapshot_block = View.snapshot_block_of_cycle cycle in
      let staking_balance, delegators, _ =
        compute_rights_at_block Tztypes.(Block_level snapshot_block) delegate in
      let normalized = List.map (fun (kt1, b) -> (kt1, b /. staking_balance)) delegators in
      List.map (fun (kt1, perc) ->
          let reward_for_contract = (perc *. float_rewards_for_delegators) in
          (kt1, reward_for_contract)
        ) normalized
end
