(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_client_alpha.Proto_alpha

let public_key_hash_from_string (s : string) =
  Signature.Public_key_hash.of_b58check_exn s

let main () =
  match Cmdline.commandline_outcome with
  | Compute_rewards { delegate ;
                      cycle ;
                      compute_rewards_opts = { rpc_config } } ->
      (* Logs.set_level (Some App) ;
       * Logs.set_reporter (Logs_fmt.reporter ()) ;
       * Logs.app (fun m -> m "Connecting to node %s:%d"
       *              rpc_config.host rpc_config.port) ; *)
      let delegate = public_key_hash_from_string delegate in
      let cctxt =
        Compute_rewards.init_cctxt
          "/tmp/tezos-node.cMWFGKRS"
          `Main
          rpc_config in
      Lwt_main.run begin
        let uri = Uri.of_string "tezos-rewards://" in
        Internal_event.All_sinks.activate uri >>=? fun () ->
        Compute_rewards.compute_rewards `Main delegate cycle 0.9 cctxt >>=? fun list ->
        iter_s (fun (contract, tez) ->
            let ct = Alpha_context.Contract.to_b58check contract in
            let tez = Alpha_context.Tez.to_string tez in
            Logging.log (Printf.sprintf "%s -> %s" ct tez)
          ) list >>=? fun ()->
        Compute_rewards.get_roll_snapshot_for_cycle `Main cycle cctxt >>=? fun roll_snap ->
        Logging.log (Printf.sprintf "Roll snapshot: %d" roll_snap)
      end
  | No_command -> assert false

let _ =
  let res = main () in
  match res with
  | Ok _ -> ()
  | Error errs ->
      let log_res = Logging.log (Format.asprintf "Error: %a" pp_print_error errs) in
      ignore (Lwt_main.run @@ log_res)


(* open Cmdliner_helpers.Convs
 * open Cmdliner_helpers.Terms
 * module OldConvs = Old_alpha_cmdliner_helpers.Convs
 *
 * let delegate_pay_cmd =
 *   let open OldConvs in
 *   let default_addr = Signature.Public_key_hash.of_b58check_exn
 *       "tz1KtvGSYU5hdKD288a1koTBURWYuADJGrLE" in
 *   let default_url = Uri.make ~host:"localhost" ~port:8732 () in
 *   let delegate_addr =
 *     Arg.(value & opt address default_addr & info ["d" ; "delegate"] ~docv:"PKH") in
 *   let fee_pct =
 *     Arg.(value & opt float 0.90 & info ["fee"] ~docv:"%FEE") in
 *   let account =
 *     let doc = "Account to use on Ledger (44'/1729'/<account>/<address>)" in
 *     Arg.(value & opt int32 0l & info ["account"] ~doc ~docv:"ACCOUNT#") in
 *   let address =
 *     let doc = "Address to use on Ledger. (44'/1729'/<account>/<address>)" in
 *     Arg.(value & opt int32 0l & info ["address"] ~doc ~docv:"ADDRESS#") in
 *   let cycle =
 *     Arg.(required & pos 0 (some int) None & info [] ~docv:"CYCLE") in
 *   let snapshot_idx =
 *     Arg.(required & pos 1 (some int) None & info [] ~docv:"SNAPSHOT_ID") in
 *   Term.(const delegate_pay $ tezos_client_dir $
 *         uri ~default:default_url ~args:["tezos-url"] ~doc:"URL of a Tezos node" $
 *         delegate_addr $ cycle $ fee_pct $ account $
 *         address $ snapshot_idx $ setup_log)
 *
 * let delegate_pay_info =
 *   Term.info ~doc:"Pay rewards to deleguees" "delegate-pay"
 *
 * let lwt_run v =
 *   Lwt.async_exception_hook := begin fun exn ->
 *     Logs.err (fun m -> m "%a" pp_exn exn) ;
 *   end ;
 *   match Lwt_main.run v with
 *   | Error err ->
 *       Logs.err (fun m -> m "%a" pp_print_error err) ;
 *       exit 1
 *   | Ok () -> ()
 *
 * let () = match Term.eval
 *                  (Term.((const lwt_run) $ delegate_pay_cmd),
 *                   delegate_pay_info) with
 * | `Error _ -> exit 1
 * | #Term.result -> exit 0 *)
