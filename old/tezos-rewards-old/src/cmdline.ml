(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* options:
   node uri (default: localhost:8732)

   parameters:
   delegate
   fee
   ledger-account
   ledger-address
   cycle *)

(*---------------------------------------------------------------------------*)
(* Typedefs *)

type rewards_options = {
  rpc_config : RPC_client.config ;
}

type command =
  | No_command
  | Compute_rewards of { delegate : string ;
                         cycle : int ;
                         compute_rewards_opts : rewards_options }

(* -------------------------------------------------------------------------- *)
(* Global state set by command line parsing. *)

let commandline_outcome_ref : command option ref = ref None

(* -------------------------------------------------------------------------- *)
(* Handling the options *)

let split_uri (uri : string) =
  match String.split_on_char ':' uri with
  | [] | _ :: [] | _ :: _ :: _ :: _ ->
      None
  | addr :: uri :: [] ->
      Some (addr, uri)

let default_node_address, default_node_port = "localhost", "8732"

let badly_formatted_port str =
  Printf.eprintf "Badly formatted uri %s" str ;
  exit 1

let badly_formatted_cycle str =
  Printf.eprintf "Badly formatted cycle %s" str ;
  exit 1

let compute_rewards_handler
    (node_address_opt : string option)
    (delegate : string)
    (cycle : string) () =
  let rpc_config =
    match node_address_opt with
    | None -> RPC_client.default_config
    | Some uri ->
        let host, port =
          match split_uri uri with
          | None -> badly_formatted_port uri
          | Some (host, port) -> (host, port) in
        let port = try int_of_string port with
          | Failure _ -> badly_formatted_port uri in
        { RPC_client.default_config with host ; port } in
  let cycle = try int_of_string cycle with
    | Failure _ ->
        badly_formatted_cycle cycle in
  let options = { rpc_config } in
  commandline_outcome_ref := (Some (Compute_rewards { delegate ;
                                                      cycle ;
                                                      compute_rewards_opts = options })) ;
  return ()

module Delegate_pay_cmd =
struct
  let default_url = Uri.make ~host:"localhost" ~port:8732 ()

  module Options =
  struct
    let node_address =
      let node_address_param =
        Clic.parameter (fun (_ : unit) parsed -> return parsed) in
      Clic.arg
        ~doc:"Address of the node\n"
        ~long:"node-address"
        ~placeholder:"uri"
        node_address_param
  end

  let options =
    let open Options in
    Clic.args1
      node_address

  let params =
    Clic.(prefixes [ "compute" ; "rewards" ; "for" ; "delegate" ] @@
          string
            ~name:"DELEGATE"
            ~desc:"Address of delegate" @@
          prefixes [ "at" ; "cycle" ] @@
          string
            ~name:"CYCLE"
            ~desc:"Cycle at which we wish to compute rewards" @@
          stop)

  let command =
    Clic.command
      ~desc:"Compute rewards"
      options
      params
      compute_rewards_handler
end

let usage () =
  Clic.usage Format.std_formatter
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options:Clic.no_options
    [ Delegate_pay_cmd.command ]

let _ =
  let args = List.tl (Array.to_list Sys.argv) in
  let result =
    Lwt_main.run begin
      Clic.parse_global_options Clic.no_options () args >>=? fun ((), args) ->
      Clic.dispatch [ Delegate_pay_cmd.command ] () args >>=? fun () ->
      return args
    end in
  match result with
  | Ok _global_opts -> ()
  | Error _errors -> usage () ; exit 1

let commandline_outcome =
  match !commandline_outcome_ref with
  | None -> No_command
  | Some command -> command
