(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)


module Event =
  Internal_event.Make(struct
    type t = string

    include Internal_event.Event_defaults

    let name = "tezos-rewards"
    let doc  = "Messages to the user produced by tezos-rewards"
    let pp   = Format.pp_print_string
    let encoding = Data_encoding.string
  end)

module Stdout_sink : Internal_event.SINK =
struct
  type t = unit
  let uri_scheme = "tezos-rewards"
  let configure _ = return ()
  let handle : type a. t -> a Internal_event.event_definition ->
    ?section: Internal_event.Section.t -> (unit -> a) -> unit tzresult Lwt.t =
    fun (type a) () (ev_def : a Internal_event.event_definition) ?section f ->
      ignore section ;
      let module Ev = (val ev_def) in
      Format.printf "%a\n%!" Ev.pp (f ()) ;
      return_unit
  let close () = return_unit
end

let () =
  Internal_event.All_sinks.register (module Stdout_sink)

let log msg =
  Event.emit (fun () -> msg)
