(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_client_alpha.Proto_alpha

module Unix_client = Tezos_client_base_unix.Client_context_unix

(* -------------------------------------------------------------------------- *)
(* Typedefs *)

type constants = {
  preserved_cycles : int ;
  blocks_per_cycle : int ;
  blocks_per_roll_snapshot : int
}

type head_info = {
  current_cycle : int ;
  current_cycle_position : int ;
  current_level : int
}

type 'mutez rights_info = {
  staking_balance : 'mutez ;
  delegators : (Alpha_context.Contract.t * 'mutez) list ;
  delegated_balance : 'mutez
}

type error += Unexpected_structure_of_context
type error += Cycle_not_present
type error += Invalid_parameter

(* -------------------------------------------------------------------------- *)
(* Convenience *)
let last_block_of_cycle (cycle_length : int) (cycle : int) =
  (cycle + 1) * cycle_length

let first_block_of_cycle (cycle_length : int) (cycle : int) =
  cycle * cycle_length + 1

(* Extract value out of 'a tzresult Lwt.t *)
let run (x : 'a tzresult Lwt.t) : 'a =
  match Lwt_main.run x with
  | Ok x       -> x
  | Error errs ->
      let errmsg =
        Format.asprintf "run: %a" pp_print_error errs in
      Pervasives.failwith errmsg

(* -------------------------------------------------------------------------- *)
(* Connect to the node *)

let connect
    (base_dir : string)
    (chain : Chain_services.chain)
    (rpc_config : RPC_client.config) : #Unix_client.unix_full =
  new Unix_client.unix_full
    ~base_dir
    ~chain
    ~block:(`Head 0)
    ~confirmations:None
    ~password_filename:None
    ~rpc_config

(* Initialize context *)
let init_cctxt base_dir chain rpc_config =
  let unix_ctxt = connect base_dir chain rpc_config in
  new wrap_full unix_ctxt

(* Get protocol constants at given block. *)
let get_constants_at_block
    (chain : Chain_services.chain)
    (block : Block_services.block)
    (cctxt : wrap_full) =
  Constants_services.all cctxt (chain, block) >>=? fun constants ->
  match constants with | { fixed = _ ; parametric } ->
    (match parametric with
     | { preserved_cycles ;
         blocks_per_cycle ;
         blocks_per_roll_snapshot ; _ } ->
         let blocks_per_cycle =
           Int32.to_int blocks_per_cycle in
         let blocks_per_roll_snapshot =
           Int32.to_int blocks_per_roll_snapshot in
         return { preserved_cycles ;
                  blocks_per_cycle ;
                  blocks_per_roll_snapshot })

(* Get current cycle, current cycle position and current level. *)
let get_head_info
    (chain : Chain_services.chain)
    (cctxt : wrap_full) =
  Alpha_services.Helpers.current_level cctxt  (chain, `Head 0)
  >>=? fun { level ; cycle ; cycle_position ; _ } ->
  let current_cycle = Int32.to_int @@ Alpha_context.Cycle.to_int32 cycle in
  let current_cycle_position = Int32.to_int cycle_position in
  let current_level = Int32.to_int @@ Alpha_context.Raw_level.to_int32 level in
  return { current_cycle ;
           current_cycle_position ;
           current_level }

(* Get the cycles for which we remember the snapshot block, at a given block. *)
let get_cycles_having_snapshot
    (chain : Chain_services.chain)
    (block : Block_services.block)
    (cctxt : wrap_full) =
  Alpha_block_services.Context.read
    ~chain ~block cctxt [ "cycle" ] >>=? fun raw_context ->
  match raw_context with
  | Block_services.Key _ | Block_services.Cut   ->
      fail Unexpected_structure_of_context
  | Block_services.Dir subtrees ->
      map_s (fun (cycle_str, _) ->
          try return (int_of_string cycle_str) with
          | Failure _ -> fail Unexpected_structure_of_context
        ) subtrees

(* Get the block at which the snapshot happened for a given cycle. *)
let get_roll_snapshot_for_cycle
    (chain : Chain_services.chain)
    (cycle : int)
    (cctxt : wrap_full) =
  get_constants_at_block chain (`Head 0) cctxt >>=? fun
    { blocks_per_cycle ; blocks_per_roll_snapshot ; _ } ->
  let first_block_height =
    first_block_of_cycle blocks_per_cycle cycle in
  let first_block =
    `Level (Int32.of_int first_block_height) in
  get_cycles_having_snapshot chain first_block cctxt >>=? fun cycles ->
  if not (List.mem cycle cycles) then
    fail Cycle_not_present
  else
    let c = string_of_int cycle in
    Alpha_block_services.Context.read
      ~chain ~block:first_block ~depth:1 cctxt [ "cycle" ; c ; "roll_snapshot" ]
    >>=? function
    | Block_services.Dir _
    | Block_services.Cut   ->
        fail Unexpected_structure_of_context
    | Block_services.Key int_bytes ->
        let open Data_encoding in
        match Binary.of_bytes Data_encoding.int16 int_bytes with
        | None -> fail Unexpected_structure_of_context
        | Some snapshot_index ->
          let snapshot_block_for_cycle cycle index =
            (first_block_of_cycle blocks_per_cycle (cycle - 7))
            + (index + 1) * blocks_per_roll_snapshot in
          return (snapshot_block_for_cycle cycle snapshot_index)

let contract_of_hash (c : Contract_hash.t) =
  let open Alpha_context in
  Alpha_environment.wrap_error @@
  Contract.of_b58check (Contract_hash.to_b58check c)

(* Get the (unnormalized) distribution of stake at a given block. *)
let compute_rights_at_block
    (chain : Chain_services.chain)
    (block : Block_services.block)
    (delegate : Signature.public_key_hash)
    (cctxt : wrap_full) =
  Delegate_services.info cctxt (chain, block) delegate >>=? fun
    { staking_balance ; delegated_contracts ; delegated_balance ; _ } ->
  map_s (fun contract_hash ->
      Lwt.return (contract_of_hash contract_hash) >>=? fun c ->
      Contract_services.balance cctxt (chain, block) c >>=? fun b ->
      return (c, b)
    ) delegated_contracts >>=? fun delegators ->
  return {
    staking_balance ;
    delegators ;
    delegated_balance }

let mutez_to_float (tez : Alpha_context.Tez.tez) : float =
  let open Alpha_context in
  Int64.to_float (Tez.to_mutez tez)

let float_to_mutez float : Alpha_context.Tez.tez =
  let open Alpha_context in
  match Tez.of_mutez (Int64.of_float float) with
  | Some mutez -> mutez
  | None -> Pervasives.failwith "float_to_mutez"

let rights_info_to_float
    (rights_info : Alpha_context.Tez.tez rights_info) : float rights_info =
  let staking_balance = mutez_to_float rights_info.staking_balance in
  let delegators =
    List.map
      (fun (c, tez) -> (c, mutez_to_float tez))
      rights_info.delegators in
  let delegated_balance = mutez_to_float rights_info.delegated_balance in
  { staking_balance ; delegators ; delegated_balance }

(* Normalize stake *)
let normalize_stake :
  float rights_info -> (Alpha_context.Contract.t * float) list =
  fun rights_info ->
    List.map
      (fun (c, tez) -> (c, tez /. rights_info.staking_balance))
      rights_info.delegators

let cycle_of_int (i : int) =
  let open Alpha_context.Cycle in
  add root i

(* Rewards effectively obtained from [cycle].
   These rewards are released at the last block of [cycle + preserved_cycles].
   They are effectively observable at the first block of [cycle+1]. *)
let rewards_of_cycle
    (chain : Chain_services.chain)
    (delegate : Signature.public_key_hash)
    (cycle : int)
    (constants : constants)
    (cctxt : wrap_full) =
  let first_block_of_next_cycle =
    first_block_of_cycle constants.blocks_per_cycle (cycle + 1) in
  let block = `Level (Int32.of_int first_block_of_next_cycle) in
  Delegate_services.frozen_balance_by_cycle cctxt (chain, block) delegate >>=? fun cycle_map ->
  match Alpha_context.Cycle.Map.find_opt (cycle_of_int cycle) cycle_map with
  | None -> fail Cycle_not_present
  | Some { (* deposit ;  *)fees ; rewards ; _ } ->
      Lwt.return (Alpha_environment.wrap_error Alpha_context.Tez.(fees +? rewards))

(* let get_delegates_at_block
 *     (chain : Chain_services.chain)
 *     (block : Block_services.block)
 *     (delegate : Signature.public_key_hash)
 *     (cctxt : wrap_full) =
 *   Delegate_services.info cctxt (chain, block) delegate >>=? fun info ->
 *   match info with
 *   | { delegated_contracts ; delegated_balance ; staking_balance ; _ } ->
 *       Delegate_services.info cctxt blkid_plus_one delegate_addr >>=? fun info_plus_one ->
 *       (\* Get total rewards + fees at requested cycle *\)
 *       begin match Cycle.Map.find_opt cycle info_plus_one.frozen_balance_by_cycle with
 *         | None -> return Tez.zero
 *         | Some { deposit ; fees ; rewards } ->
 *             Logs.app (fun m -> m "%a %a %a" Tez.pp deposit Tez.pp fees Tez.pp rewards) ;
 *             Lwt.return (Alpha_environment.wrap_error Tez.(fees +? rewards))
 *       end >>=? fun full_reward ->
 *       Logs.app (fun m -> m "Total reward to distribute: %a" Tez.pp full_reward) ;
 *       (\* Compute how to split this reward *\)
 *       (\* Look `preserved_cycles` + 2 blocks before `cycle_int`, 16 snapshots. *\)
 *       map_s begin fun c ->
 *         Lwt.return (Alpha_environment.wrap_error
 *                       (Contract.of_b58check (Contract_hash.to_b58check c)))
 *       end delegated_contracts >>=? fun delegated_contracts ->
 *       Logs.app (fun m -> m "Found delegated contracts: @[<0>%a@]"
 *                    (Format.pp_print_list ~pp_sep:Format.pp_print_space Contract.pp)
 *                    delegated_contracts) ;
 *       map_s begin fun c ->
 *         Logs.info (fun m -> m "Get snapshots balance for %a" Contract.pp c) ;
 *         map_p begin fun (chain, block) ->
 *           (\* Alpha_block_services.hash cctxt ~chain ~block () >>=? fun blkh -> *\)
 *           (\* Logs.app (fun m -> m "Snapshot hash %a" Block_hash.pp blkh) ; *\)
 *           protect
 *             ~on_error:(fun _ -> return Tez.zero)
 *             (fun () -> Alpha.Context.get_balance cctxt ~chain ~block c)
 *         end blkid_snapshots
 *       end delegated_contracts >>=? fun balances ->
 *       let all_balances =
 *         List.map (fun lb -> List.nth lb snapshot_idx) balances in
 *       sum_tezs all_balances >>=? fun all_balances_sum ->
 *       let kmap =
 *         List.fold_left2 begin fun a c avgb ->
 *           (c, List.nth avgb snapshot_idx) :: a
 *         end [] delegated_contracts balances in
 *       let kmap = List.sort begin fun (_, avgb1) (_, avgb2) ->
 *           Tez.compare avgb2 avgb1 end kmap in
 *       let destinations =
 *         List.map begin fun (k, avgb) ->
 *           let k =
 *             match List.assoc_opt k redirections with
 *             | None -> k
 *             | Some redirect -> redirect in
 *           let avgbf = Int64.to_float (Obj.magic avgb : int64) in
 *           let staking_balancef =
 *             Int64.to_float (Obj.magic staking_balance : int64) in
 *           let pct = avgbf /. staking_balancef in
 *           let individual_reward_mutez =
 *             Int64.to_float (Obj.magic full_reward : int64) *. pct in
 *           let fee_pct =
 *             match List.assoc_opt k members with
 *             | None -> fee_pct
 *             | Some fee -> fee in
 *           let individual_reward_mutez_minus_fees =
 *             individual_reward_mutez *. fee_pct in
 *           Logs.app begin fun m ->
 *             m "%a %.2f\t%a %.2f %.2f"
 *               Contract.pp k
 *               (individual_reward_mutez /. 1e6)
 *               Tez.pp avgb
 *               pct
 *               (individual_reward_mutez_minus_fees /. 1e6)
 *           end ;
 *           let amount =
 *             (Obj.magic (Int64.of_float individual_reward_mutez_minus_fees)) in
 *           Mezos_transfer.create_destination ~destination:(Obj.magic k) ~amount ()
 *         end kmap in
 *       Logs.app begin fun m ->
 *         m "The staking balance is %a" Tez.pp staking_balance
 *       end ;
 *       Logs.app begin fun m ->
 *         m "The delegated balance is %a" Tez.pp delegated_balance
 *       end ;
 *       Logs.app begin fun m ->
 *         m "The computed delegated balance is %a" Tez.pp all_balances_sum
 *       end ;
 *       match List.filter
 *               (fun { Mezos_transfer.amount } -> amount > Obj.magic 10_000L)
 *               destinations with
 *       | [] -> return_unit
 *       | destinations ->
 *           Mezos_transfer.forge_transfer cctxt (Obj.magic hot_wallet)
 *             destinations >>=? fun { payload ; fees; gas } ->
 *           Logs.app begin fun m ->
 *             m "Operation created with fees %a, gas %a"
 *               Tez.pp (Obj.magic fees) Z.pp_print gas
 *           end ;
 *           let path = [ h 44l ; h 1729l ; h account ; h node ] in
 *           let hidapi = Hidapi.open_id_exn ~vendor_id:0x2C97 ~product_id:0x0001 in
 *           sign_and_inject_with_ledger hidapi Ed25519 cctxt path payload >>=? fun _oh ->
 *           return_unit *)

let print_constants (constants : constants) =
  let { preserved_cycles ;
        blocks_per_cycle ;
        blocks_per_roll_snapshot } = constants in
  Logging.log
    (Printf.sprintf
       "Current chain constants:\npreserved_cycles=%d\nblocks_per_cycle=%d\nblocks_per_roll_snapshot=%d"
       preserved_cycles blocks_per_cycle blocks_per_roll_snapshot)

let print_head_info (head_info : head_info) =
  let { current_cycle; current_cycle_position; current_level } = head_info in
  Logging.log
    (Printf.sprintf
       "Current level: %d\nCurrent cycle: %d\nCurrent cycle position: %d"
       current_level current_cycle current_cycle_position)

let print_rewards_info delegate cycle chain =
  let delegate = Signature.Public_key_hash.to_b58check delegate in
  Logging.log
    (Printf.sprintf "Computing rewards due by delegate %s at cycle %d on chain %s"
       delegate cycle (Chain_services.to_string chain))

let compute_rewards
    (chain : Chain_services.chain)
    (delegate : Signature.public_key_hash)
    (cycle : int)
    (percentage : float)
    (cctxt : wrap_full) =
  let head = `Head 0 in
  get_constants_at_block chain head cctxt >>=? fun constants ->
  get_head_info chain cctxt >>=? fun head_info ->
  print_constants constants >>=? fun () ->
  print_head_info head_info >>=? fun () ->
  if cycle >= head_info.current_cycle then
    fail Cycle_not_present
  else if percentage < 0.0 || percentage >= 1.0 then
    fail Invalid_parameter
  else
    rewards_of_cycle chain delegate cycle constants cctxt >>=? fun rewards ->
    let float_rewards_for_delegators =
      percentage *. (mutez_to_float rewards) in
    get_roll_snapshot_for_cycle chain cycle cctxt >>=? fun snapshot_block ->
    compute_rights_at_block
      chain (`Level (Int32.of_int snapshot_block)) delegate cctxt >>=? fun tez_rights ->
    let float_rights = rights_info_to_float tez_rights in
    let normalized   = normalize_stake float_rights in
    return begin
      List.map (fun (contract, p) ->
          let reward_for_contract =
            float_to_mutez (p *. float_rewards_for_delegators) in
          (contract, reward_for_contract)
        ) normalized
    end
