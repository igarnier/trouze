(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2018 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let sign_raw : seed:Types.ed25519_sk -> data:string -> Types.ed25519_sig =
  fun ~seed ~data ->
    let module Sign = Sodium.Sign in
    let skey, _ = Sign.seed_keypair (Sign.Bytes.to_seed (Bytes.of_string (seed :> string))) in
    let signature = Sign.Bytes.sign_detached skey (Bytes.of_string data) in
    let result = Bytes.to_string (Sign.Bytes.of_signature signature) in
    Types.ed25519_sig result

let sign_with_b58encoded_seed : b58_seed:string -> data:string -> Types.ed25519_sig =
  fun ~b58_seed ~data ->
    let bin_skey = B58check.skey_of_b58 b58_seed in
    sign_raw ~seed:bin_skey ~data

let sign : b58_seed:string -> watermark:string -> data:string -> Types.ed25519_sig =
  fun ~b58_seed ~watermark ~data ->
    let watermarked = watermark ^ data in
    let module Hash = Sodium.Generichash in
    let hashed_watermarked =
      Hash.Bytes.digest ~size:32 (Bytes.of_string watermarked)
      |> Hash.Bytes.of_hash
      |> Bytes.to_string in
    sign_with_b58encoded_seed ~b58_seed ~data:hashed_watermarked
