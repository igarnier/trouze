type frozen_balance_on_cycle = {
  cycle : int ;
  deposit : int ;
  fees : int ;
  rewards : int
}

let frozen_balance_on_cycle_from_json json =
  match json with
  | `Assoc ([("cycle", `Int cycle);
             ("deposit", `String deposit);
             ("fees", `String fees);
             ("rewards", `String rewards)]) ->
    { cycle ;
      deposit = int_of_string deposit ;
      fees = int_of_string fees ;
      rewards = int_of_string rewards  }
  | _ ->
    failwith "frozen_balance_on_cycle_from_json"

type delegate =
  { balance : int ;
    frozen_balance : int ;
    frozen_balance_by_cycle : frozen_balance_on_cycle list ;
    staking_balance : int ;
    delegated_contracts : Types.pkh list ;
    delegated_balance : int ;
    deactivated : bool ;
    grace_period : int ;
  }

let delegate_from_json json =
  match json with
  | `Assoc ([("balance", `String balance);
             ("frozen_balance", `String frozen_balance);
             ("frozen_balance_by_cycle",`List frozen);
             ("staking_balance", `String staking_balance);
             ("delegated_contracts",`List delegated_contracts);
             ("delegated_balance", `String delegated_balance);
             ("deactivated", `Bool deactivated);
             ("grace_period", `Int grace_period)]) ->
    { balance = int_of_string balance ;
      frozen_balance = int_of_string frozen_balance ;
      frozen_balance_by_cycle =
        List.map frozen_balance_on_cycle_from_json frozen ;
      staking_balance = int_of_string staking_balance ;
      delegated_contracts =
        List.map (function `String s -> Types.pkh s | _ -> assert false) delegated_contracts ;
      delegated_balance = int_of_string delegated_balance ;
      deactivated = deactivated ;
      grace_period = grace_period }
  | _ ->
    failwith "delegate_from_json"

type level =
  { level : int ;
    level_position : int ;
    cycle : int ;
    cycle_position : int ;
    voting_period : int ;
    voting_period_position : int ;
    expected_commitment : bool }

let level_of_json json =
  match json with
  | `Assoc ([ ("level", `Int level) ;
              ("level_position", `Int level_position) ;
              ("cycle", `Int cycle) ;
              ("cycle_position", `Int cycle_position);
              ("voting_period", `Int voting_period);
              ("voting_period_position", `Int voting_period_position);
              ("expected_commitment", `Bool expected_commitment) ]) ->
    { level ;
      level_position ;
      cycle ;
      cycle_position ;
      voting_period ;
      voting_period_position ;
      expected_commitment }
  | _ ->
    failwith "level_of_json"
