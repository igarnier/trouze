(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2018 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* copy-pasted from lib_crypto/base58.ml *)

type t = string

let base = 58
let zbase = Z.of_int base

module Alphabet = struct

  type t = { encode: string ; decode: string }

  let make alphabet =
    if String.length alphabet <> base then
      invalid_arg "Base58: invalid alphabet (length)" ;
    let str = Bytes.make 256 '\255' in
    for i = 0 to String.length alphabet - 1 do
      let char = int_of_char alphabet.[i] in
      if Bytes.get str char <> '\255' then
        Format.kasprintf invalid_arg
          "Base58: invalid alphabet (dup '%c' %d %d)"
          (char_of_int char) (int_of_char @@ Bytes.get str char) i ;
      Bytes.set str char (char_of_int i) ;
    done ;
    { encode = alphabet ; decode = Bytes.to_string str }

  let bitcoin =
    make "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
  let ripple =
    make "rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz"
  let flickr =
    make "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"

  let default = bitcoin

  (* let all_in_alphabet alphabet string =
   *   let ok = Array.create ~len:256 false in
   *   String.iter ~f:(fun x -> ok.(Char.code x) <- true) alphabet.encode ;
   *   let res = ref true in
   *   for i = 0 to (String.length string) - 1 do
   *     res := !res && ok.(Char.code string.[i])
   *   done;
   *   !res *)

  let pp ppf { encode ; _ } = Format.fprintf ppf "%s" encode

end


let b58chars = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
let b58base = String.length b58chars

let of_char ?(alphabet=Alphabet.default) x =
  let pos = String.get alphabet.decode (int_of_char x) in
  match pos with
  | '\255' -> None
  | _ -> Some (int_of_char pos)

let to_char ?(alphabet=Alphabet.default) x =
  alphabet.encode.[x]

let count_trailing_char s c =
  let len = String.length s in
  let rec loop i =
    if i < 0 then len
    else if String.get s i <> c then (len-i-1)
    else loop (i-1) in
  loop (len-1)

let count_leading_char s c =
  let len = String.length s in
  let rec loop i =
    if i = len then len
    else if String.get s i <> c then i
    else loop (i+1) in
  loop 0

let decode ?(alphabet=Alphabet.default) s =
  let open Base in
  String.fold ~init:(Some Z.zero) ~f:begin fun a c ->
    match a, of_char ~alphabet c with
    | Some a, Some i -> Some Z.(add (of_int i) (mul a zbase))
    | _ -> None
  end s |>
  Option.map ~f:begin fun res ->
    let res = Z.to_bits res in
    let res_tzeros = count_trailing_char res '\000' in
    let len = String.length res - res_tzeros in
    let zeros = count_leading_char s alphabet.encode.[0] in
    String.make zeros '\000' ^
    String.init len ~f:(fun i -> String.get res (len - i - 1))
  end

let encode ?(alphabet=Alphabet.default) s =
  let len = String.length s in
  let s = String.init len (fun i -> String.get s (len - i - 1)) in
  let zero = alphabet.encode.[0] in
  let zeros = count_trailing_char s '\000' in
  let res_len = (len * 8 + 4) / 5 in
  let res = Bytes.make res_len '\000' in
  let s = Z.of_bits s in
  let rec loop s =
    if s = Z.zero then 0 else
      let s, r = Z.div_rem s zbase in
      let i = loop s in
      Bytes.set res i (to_char ~alphabet (Z.to_int r)) ;
      i + 1 in
  let i = loop s in
  let res = Bytes.sub_string res 0 i in
  String.make zeros zero ^ res


let checksum data =
  let module H = Digestif.SHA256 in
  let hash = H.digest_string data in
  let s = Digestif.SHA256.to_raw_string hash in
  Digestif.SHA256.to_raw_string @@ H.digest_string s

let signature_to_b58 (signature : Types.ed25519_sig) =
  let prefix = "\x09\xf5\xcd\x86\x12" in
  let data   = prefix ^ (signature :> string) in
  let checks = String.sub (checksum data) 0 4 in
  encode (data ^ checks)

let skey_of_b58 (b58_skey : string) : Types.ed25519_sk =
  let prefix_bytes = 4 in
  let sig_bytes    = 32 in
  let decoded_bin  =
    match decode b58_skey with
    | None -> failwith "skey_of_b58: invalid b58 string"
    | Some x -> x in
  let length_bytes = String.length decoded_bin in
  let prefixless   =
    String.sub decoded_bin prefix_bytes (length_bytes - prefix_bytes) in
  let result = String.sub prefixless 0 sig_bytes in
  Types.ed25519_sk result
