module R = Rpc

let _ =
  R.set_uri ~host:"http://alphanet.tzscan.io:8732"

let tz1 =
  "tz3gN8NTLNLJg5KRsUU47NHNVHbdhcFXjjaB"

let cycle_length = 2048

let last_block_of_cycle (cycle : int) =
  (cycle + 1) * cycle_length

let first_block_of_cycle (cycle : int) =
  cycle * cycle_length + 1

let (/) = (@)

let blocks = ["chains";"main";"blocks"]

let block (height : int) = blocks / [ string_of_int height ]

let context (height : int) =
  block height / [ "context" ]

let contracts (height : int) =
  context height / [ "contracts" ]

let delegate (height : int) =
  (context height / [ "delegates" ; tz1 ])

let delegated_contracts (height : int) =
  delegate height / ["delegated_contracts"]

let delegate_balance (height : int) (kt1 : string) =
  contracts height / [ kt1 ] / [ "balance" ]

(* get info on delegate *)
let get_delegate height =
  Rpc.http_get @@ delegate height

let get_delegated_contracts height =
  Rpc.http_get @@ delegated_contracts height

type balance_update =
  | Contract of { contract : string ;
                  change   : Int64.t }
  | Freezer of { category : category ;
                 delegate : string ;
                 level    : int ;
                 change   : Int64.t }
[@@deriving show]

and category =
    | Deposits
  | Rewards
  | Fees
[@@deriving show]


type cycle_info = {
  cycle       : int ;
  staking_balance : Int64.t ;
  delegators  : (string * Int64.t) list ; (* delegator * balance in mutez of delegator at start of cycle *)
  balance_updates : balance_update list
}
[@@deriving show]

let compute_share (cycle_info : cycle_info) : (string * float) list =
  List.map (fun (delegator, stake) ->
      let p = Int64.to_float stake /. Int64.to_float cycle_info.staking_balance in
      (delegator, p)
    ) cycle_info.delegators

module SMap = Map.Make(String)

type rewards = float SMap.t (* floats to be interpreted in mutez *)

let add_reward (r : rewards) (delegator : string) (mutez : float) =
  match SMap.find_opt delegator r with
  | Some rewards ->
    SMap.add delegator (rewards +. mutez) r
  | None ->
    SMap.add delegator mutez r

let show_rewards (rewards : rewards) : string =
  let bindings = SMap.bindings rewards in
  bindings
  |> List.map (fun (d, r) -> (d, Int64.(to_string (of_float r))))
  |> List.map (fun (d, r) -> Printf.sprintf "%s gets %s mutez\n" d r)
  |> String.concat "\n"

let rewards_of_cycle (cycle : cycle_info) =
  List.fold_left (fun rewards balance_update ->
      match balance_update with
      | Freezer { category = Rewards ; change ; _ } ->
        Int64.(add rewards (neg change))
      | _ -> rewards
    ) 0L cycle.balance_updates


let compute_rewards : cycle_info list -> rewards option =
  function
  | [] | _ :: [] -> None
  | first_cycle :: rest ->
    let rewards, _ =
      List.fold_left (fun (rewards, prev_cycle) cycle ->
          let prev_rewards = Int64.to_float @@ rewards_of_cycle prev_cycle in
          let cycle_shares = compute_share cycle in
          let updated_rewards =
            List.fold_left (fun rewards (delegator, percentage) ->
                add_reward rewards delegator (percentage *. prev_rewards)
              ) rewards cycle_shares in
          (updated_rewards, cycle)
        ) (SMap.empty, first_cycle) rest in
    Some rewards

let balance_update_from_json (json : Yojson.Safe.t) =
  match json with
  | `Assoc fields ->
    let kind =
      List.assoc "kind" fields
      |> Yojson.Safe.Util.to_string in
    (match kind with
     | "contract" ->
       let contract =
         List.assoc "contract" fields
         |> Yojson.Safe.Util.to_string in
       let change   =
         List.assoc "change" fields
         |> Yojson.Safe.Util.to_string
         |> Int64.of_string in
       Contract { contract ; change }
     | "freezer" ->
       let category =
         let category =
           List.assoc "category" fields |> Yojson.Safe.Util.to_string in
         match category with
         | "deposits" -> Deposits
         | "rewards" -> Rewards
         | "fees"    -> Fees
         | c         -> failwith @@ "balance_update_from_json: " ^ c in
       let delegate =
         List.assoc "delegate" fields
         |> Yojson.Safe.Util.to_string
       in
       let level    =
         List.assoc "level" fields
         |> Yojson.Safe.Util.to_int in
       let change   =
         List.assoc "change" fields
         |> Yojson.Safe.Util.to_string
         |> Int64.of_string in
       Freezer { category ; delegate ; level ; change }
     | _ ->
       failwith "balance_update_from_json"
    )
  | _ ->
    failwith "balance_update_from_json"

let extract_cycles start stop =
  let cycles = Base.List.range ~start:`inclusive ~stop:`inclusive start stop in
  List.map (fun cycle ->
      let first_block = first_block_of_cycle cycle in
      let last_block  = last_block_of_cycle cycle in
      let delegated_contracts =
        get_delegated_contracts first_block
        |> Yojson.Safe.Util.to_list
        |> Yojson.Safe.Util.filter_string in
      let delegated_balances =
        List.map (fun delegate ->
            let balance =
              Rpc.http_get @@ delegate_balance first_block delegate
              |> Yojson.Safe.Util.to_string
              |> Int64.of_string in
            (delegate, balance)
          ) delegated_contracts in
      let staking_balance =
        Rpc.http_get @@ delegate first_block / ["staking_balance"]
        |> Yojson.Safe.Util.to_string
        |> Int64.of_string in
      let balance_updates =
        let metadata =
          Rpc.http_get @@ block last_block / ["metadata"] in
        Yojson.Safe.Util.to_assoc metadata
        |> List.assoc "balance_updates"
        |> Yojson.Safe.Util.to_list
        |> List.map balance_update_from_json
        |> List.filter (function
            | Contract _ -> false
            | Freezer { delegate ; _ } ->
              delegate = tz1
          ) in
      { cycle ;
        staking_balance ;
        delegators = delegated_balances ;
        balance_updates
      }
    ) cycles

let start, stop =
  if Array.length Sys.argv < 3 then
    (Printf.eprintf "Not enough arguments. Usage: indexer.exe start_cycle stop_cycle\nCycles bounds are inclusive.\n" ;
     exit 1)
  else
    let start = int_of_string Sys.argv.(1) in
    let stop = int_of_string Sys.argv.(2) in
    (start, stop)

let cycles = extract_cycles start stop

let rewards =
  match compute_rewards cycles with
  | None ->
    Printf.eprintf "Could not compute rewards. Not enough cycle data?" ;
    exit 1
  | Some rewards -> rewards

let _ =
  List.iter (fun cycle_info ->
      Printf.printf "%s\n" (show_cycle_info cycle_info)
    ) cycles

let _ =
  Printf.printf "Rewards:\n%s\n" (show_rewards rewards)
