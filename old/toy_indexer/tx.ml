(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2018 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type transaction =
  { source      : Types.pkh
  ; destination : Types.pkh
  ; fee         : int
  ; gas_limit   : int
  ; counter     : int
  ; storage_limit : int
  ; amount : int }

type reveal =
  { source : Types.pkh
  ; fee : int
  ; counter : int
  ; gas_limit : int
  ; storage_limit : int
  ; public_key : Types.pk }

type t =
  | Transaction of transaction
  | Reveal of reveal

type receipt =
  | Applied of balance_update list
  | Failed of error list

and balance_update =
  { kind : contract_type
  ; contract : Types.pkh
  ; change : int }

and contract_type =
  | Contract

and error = { error_kind : error_kind ; id : string  }

and error_kind =
  | Temporary

let transaction_to_json (x : transaction) =
  match x with
  | { source ; destination ; fee ; gas_limit ; counter ; storage_limit
    ; amount } ->
    `Assoc [ ("kind", `String "transaction")
           ; ("source", `String (source :> string))
           ; ("destination", `String (destination :> string))
           ; ("fee", `String (string_of_int fee))
           ; ("counter", `String (string_of_int counter))
           ; ("gas_limit", `String (string_of_int gas_limit))
           ; ("storage_limit", `String (string_of_int storage_limit))
           ; ("amount", `String (string_of_int amount)) ]

let reveal_to_json (x : reveal) =
  match x with
  | { source ; fee ; counter ; gas_limit ; storage_limit ; public_key } ->
    `Assoc [ ("kind", `String "reveal")
           ; ("source", `String (source :> string))
           ; ("fee", `String (string_of_int fee))
           ; ("counter", `String (string_of_int counter))
           ; ("gas_limit", `String (string_of_int gas_limit))
           ; ("storage_limit", `String (string_of_int storage_limit))
           ; ("public_key", `String (public_key :> string) ) ]

let to_json (x : t) =
  match x with
  | Transaction tx ->
    transaction_to_json tx
  | Reveal r ->
    reveal_to_json r

let json_error msg json =
  let pretty_printed = Yojson.Safe.pretty_to_string json in
  failwith @@ msg ^ " " ^ pretty_printed

let balance_update_from_json (balance_update : Yojson.Safe.t) =
  let module U = Yojson.Safe.Util in
  match balance_update with
  | `Assoc _ ->
    let kind   = match U.member "kind" balance_update with
      | `String "contract" -> Contract
      | _ -> json_error "balance_update_from_json: member \"kind\"" balance_update in
    let ctrct  = match U.member "contract" balance_update with
      | `String tz1 -> Types.pkh tz1
      | _ -> json_error "balance_update_from_json: member \"contract\"" balance_update in
    let change = match U.member "change" balance_update with
      | `String int -> int_of_string int
      | _ -> json_error "balance_update_from_json: member \"change\"" balance_update in
    { kind ; contract = ctrct ; change }
  | _ ->
    json_error "balance_update_from_json: expected `Assoc" balance_update

let error_from_json (error : Yojson.Safe.t) =
  let module U = Yojson.Safe.Util in
  match error with
  | `Assoc _ ->
    let kind   = match U.member "kind" error with
      | `String "temporary" -> Temporary
      | _ -> json_error "error_from_json: member \"kind\"" error in
    let id     = match U.member "id" error with
      | `String id -> id
      | _ -> json_error "error_from_json: member \"id\"" error in
    { error_kind = kind ; id }
  | _ ->
    json_error "error_from_json: expected `Assoc" error

let receipt_from_json (operation_result : Yojson.Safe.t) =
  let module U = Yojson.Safe.Util in
  match U.member "status" operation_result with
  | `String "applied" ->
    (match U.member "balance_updates" operation_result with
     | `List updates ->
       Applied (List.map balance_update_from_json updates)
     | upd ->
       json_error "receipt_from_json: expected `List updates" upd
     | exception _exn ->
       json_error "receipt_from_json: member \"balance_updates\"" operation_result)
  | `String "failed" ->
    (match U.member "errors" operation_result with
     | `List errors ->
       Failed (List.map error_from_json errors)
     | errs ->
       json_error "receipt_from_json: expected `List errors" errs
     | exception _exn ->
       json_error "receipt_from_json: member \"errors\"" operation_result)
  | _ ->
    json_error
      "receipt_from_json: expected `String \"applied|failed\""
      operation_result
  | exception _exn ->
    json_error "receipt_from_json: member \"status\"" operation_result
