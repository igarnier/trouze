(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2018 Nomadic Labs. <nomadic@tezcore.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* ------------------------------------------------------------------------- *)
(* GET and POST requests via Cohttp *)

let uri = ref None

let set_uri ~host =
  uri := Some host

let get_uri () =
  match !uri with
  | None ->
    failwith "get_uri: uri not set, aborting"
  | Some uri -> uri

let get_string_from_body body =
  match body with
  | `Stream stream ->
    let%lwt ls = Lwt_stream.to_list stream in
    (match ls with
     | [] ->
       Lwt.fail_with "Rpc.get_string_from_body: error, empty reply"
     | [ reply ] ->
       Lwt.return reply
     | replies ->
       let res = String.concat "" replies in
       Lwt.return res
       (* Lwt.fail_with "Rpc.get_string_from_body: error, cannot interpret multi-part reply" *))
  | `Strings sl ->
    Lwt.return @@ String.concat "," sl
  | `String s ->
    Lwt.return s
  | `Empty ->
    Lwt.fail_with "Rpc.get_string_from_body: empty body"

let get_json_from_body body =
  let%lwt str = get_string_from_body body in
  try Lwt.return @@ Yojson.Safe.from_string str with
  | exn ->
    Lwt_log.info "Rpc.http_post: error, string cannot be parsed to json" ;%lwt
    Lwt_log.info_f "Rpc.http_post: dumping faulty string:\n\"%s\"" str ;%lwt
    Lwt.fail exn

let http_get path =
  let str = String.concat "/" (get_uri () :: path) in
  Lwt_main.run begin
    let%lwt _resp, body = Cohttp_lwt_unix.Client.get (Uri.of_string str) in
    get_json_from_body body
  end

let http_post path data =
  let str     = String.concat "/" (get_uri () :: path) in
  let uri     = Uri.of_string str in
  let headers = Cohttp.Header.of_list [ ("Content-type", "application/json") ] in
  let body    = Cohttp_lwt.Body.of_string data in
  Lwt_main.run begin
    Lwt_log.debug_f "post body: %s\n" data ;%lwt
    let%lwt _resp, body =
      Cohttp_lwt_unix.Client.post uri ~headers ~body
    in
    get_json_from_body body
  end

(* ------------------------------------------------------------------------- *)
(* Json helpers *)

let json_error msg json =
  let pretty_printed = Yojson.Safe.pretty_to_string json in
  failwith @@ (msg ^ "\n" ^ pretty_printed)

let (-->) (x : Yojson.Safe.t) (k : string) =
  match x with
  | `Assoc l ->
    List.assoc k l
  | _ ->
    json_error ("Can't access field " ^ k) x

(* ------------------------------------------------------------------------- *)
(* Tezos RPC wrappers *)

(* Predefined RPC paths for the head block and the context of the head block. *)
let (/) = (@)
let head = ["chains";"main";"blocks";"head"]
let head_context = head / ["context"]

module Head =
struct
  let hash () =
    let json = http_get (head @ [ "hash" ]) in
    match json with
    | `String s ->
      Types.hash s
    | _ ->
      failwith "Rpc.Head.hash: invalid block hash"

  let level () =
    let json = http_get (head / [ "helpers" ; "current_level" ]) in
    Tztypes.level_of_json json

end

module Block =
struct
  let by_hash (hash : Types.hash) =
    http_get [ "chains" ; "main" ; "blocks" ; (hash:>string) ]
    (* TODO: convert to a proper type of "blocks". *)
end

module Forge =
struct
  let path = (head @ ["helpers"; "forge"; "operations"])

  let operation ~(branch : Types.hash) ~(operation : Tx.t) =
    let op_json = Tx.to_json operation in
    let json =
      `Assoc
        [ ("branch", `String (branch :> string))
        ; ("contents", `List [ op_json ]) ] in
    let result = http_post path (Yojson.Safe.to_string json) in
    match result with
    | `String s -> Types.hash s
    | _ ->
      failwith @@ ("Rpc.Forge.operation: unexpected result" ^
                   (Yojson.Safe.to_string result))
end

module Inject =
struct
  let path = [ "/injection/operation?chain=main" ]

  let operation ~data =
    let data   = Yojson.Safe.to_string (`String data) in
    let result = http_post path data in
    match result with
    | `String s -> Types.hash s
    | _ ->
      failwith @@ ("Rpc.Inject.operation: unexpected result" ^
                   (Yojson.Safe.to_string result))
end

module Helpers =
struct

  let run_operation
      ~(branch : Types.hash)
      ~(operation : Tx.t)
      ~(sign:Types.ed25519_sig) =
    let path = head @ [ "helpers" ; "scripts" ; "run_operation" ] in
    let op_json = Tx.to_json operation in
    let json = `Assoc [
        ("branch", `String (branch :> string))
      ; ("contents", `List [ op_json ])
      ; ("signature", `String (B58check.signature_to_b58 sign))
      ] in
    let data = Yojson.Safe.to_string json in
    let result = http_post path data in
    let contents_list = result --> "contents" in
    match contents_list with
    | `List [ contents ] ->
      let op_res = contents --> "metadata" --> "operation_result" in
      Tx.receipt_from_json op_res
    | _ ->
      json_error "Helpers.run_operation: expected `List [_]" contents_list

  let preapply
      ~(protocol : Types.hash)
      ~(branch: Types.hash)
      ~(operation: Tx.t)
      ~(sign : Types.ed25519_sig) =
    let path = head @ [ "helpers"; "preapply"; "operations" ] in
    let op_json = Tx.to_json operation in
    let json = `List [ `Assoc [
        ("protocol", `String (protocol :> string))
      ; ("branch", `String (branch :> string))
      ; ("contents", `List [ op_json ])
      ; ("signature", `String (B58check.signature_to_b58 sign))
      ] ] in
    let data = Yojson.Safe.to_string json in
    let result_list = http_post path data in
    match result_list with
    | `List [ result ] ->
      let contents_list = result --> "contents" in
      (match contents_list with
       | `List [ contents ] ->
         let op_res = contents --> "metadata" --> "operation_result" in
         Tx.receipt_from_json op_res
       | _ ->
         json_error "Helpers.run_operation: expected `List [_]" contents_list)
    | _ ->
      json_error "Helpers.run_operation: expected `List" result_list

end

let get_counter (contract : Types.pkh) =
  let result =
    http_get (head_context @ ["contracts"; (contract :> string); "counter"]) in
  match result with
  | `String s ->
    Some (int_of_string s)
  | _ ->
    None

let get_public_key (contract : Types.pkh) =
  let result =
    let contract :> string = contract in
    http_get (head_context @ ["contracts"; contract; "manager_key"]) in
  let key = result --> "key" in
  match key with
  | `String s ->
    Some (Types.pk s)
  | _ ->
    None
